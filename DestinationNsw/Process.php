<?php


namespace DestinationNsw;

/**
 * Class Process
 * @package DestinationNsw
 * This Class processes requests. It would be a controller in an MVC project.
 */
class Process {

  /**
   * The Service URL to be called
   * @var
   */
  private $serviceUrl;
  /**
   * The Channel ID for the request
   * @var
   */
  private $channelId;
  /**
   * The Channel Key for the request
   * @var
   */
  private $channelKey;

  /**
   * Constructor
   * @param $serviceUrl
   * @param $channelId
   * @param $channelKey
   */
  function __construct($serviceUrl, $channelId, $channelKey) {
    $this->serviceUrl = $serviceUrl;
    $this->channelId  = $channelId;
    $this->channelKey = $channelKey;

    //Create and save the channel information
    $channel       = array (new DistributionChannelType($channelId, $channelKey));
    $this->channel = $channel;

    // Create and save the SoapClient
    $client = new \SoapClient($serviceUrl, array ('soap_version' => SOAP_1_2));
    $this->client = $client;
  }

  /**
   * Gets all Providers
   * @return array
   */
  public function getProviders() {
    // Creates the search request.
    $searchRq = new ProviderSearchRq($this->channel);

    // Try to make request
    try {
      $response = $this->client->__call('ProviderOptIn', array ('CABS_ProviderOptIn_RQ' => $searchRq));
    } catch
    (\SoapFault $fault) {
      print("
            alert('Sorry, the following ERROR: " . $fault->faultcode . "-" . $fault->faultstring . ". We will now take you back to our home page.');
            window.location = 'index.php';
            ");
    }

    // Declare empty return array
    $return    = [];

    // Get the Providers Array
    $providers = $response->Channels->Channel->Providers->Provider;

    // Iterate through the providers
    foreach ($providers as $provider) {

      // Create an array of Provider objects
      $return[ ] = new Provider(
        $provider->short_name,
        $provider->content_id,
        $provider->opted_in,
        $provider->is_blocked,
        $provider->product_count,
        $provider->opted_in_effective,
        $provider->supplier_id,
        $provider->integration_mode,
        $provider->id,
        $provider->obx_id,
        $provider->supplier_obx_id,
        $provider->supplier_short_name
      );
    }

    return $return;
  }

  /**
   * Get only providers that have optedIn
   * @return array
   */
  public function optedInEffective() {
    // Get all Providers
    $all              = $this->getProviders();
    // Filter results
    $optedInEffective = array_filter($all, function (Provider $provider) {
      if (!$provider->getOptedInEffective()) {
        return FALSE;
      }
      return TRUE;
    });
    return $optedInEffective;
  }
}