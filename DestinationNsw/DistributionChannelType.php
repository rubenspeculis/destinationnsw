<?php
/**
 * Distribution Channel Type as defined in webservice structure.
 */

namespace DestinationNsw;

class DistributionChannelType {

  /**
   * @param string $id
   * @param string $key
   */
  function __construct($id = '', $key = '') {
    $this->id  = $id;
    $this->key = $key;
  }
}