<?php

namespace DestinationNsw;

/**
 * Class ProviderSearchRq
 * Sets up the object for the Search request
 * @package DestinationNsw
 */
class ProviderSearchRq {

  /**
   * @param array $channels
   * @param array $providers
   */
  function __construct(array $channels = [], array $providers = []) {
    $this->Channels = $channels;
    $this->Providers = $providers;
    $this->Query = new QuerySettings();

  }

}