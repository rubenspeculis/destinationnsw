<?php
namespace DestinationNsw;


/**
 * Class Provider
 * @package DestinationNsw
 */
class Provider {

  private $short_name;
  private $content_id;
  private $opted_in;
  private $is_blocked;
  private $product_count;
  private $opted_in_effective;
  private $supplier_id;
  private $integration_mode;
  private $id;
  private $obx_id;
  private $supplier_obx_id;
  private $supplier_short_name;

  /**
   * @param $short_name
   * @param $content_id
   * @param $opted_in
   * @param $is_blocked
   * @param $product_count
   * @param $opted_in_effective
   * @param $supplier_id
   * @param $integration_mode
   * @param $id
   * @param $obx_id
   * @param $supplier_obx_id
   * @param $supplier_short_name
   */
  function __construct($short_name, $content_id, $opted_in, $is_blocked, $product_count, $opted_in_effective, $supplier_id, $integration_mode, $id, $obx_id, $supplier_obx_id, $supplier_short_name) {
    $this->short_name          = $short_name;
    $this->content_id          = $content_id;
    $this->opted_in            = $opted_in;
    $this->is_blocked          = $is_blocked;
    $this->product_count       = $product_count;
    $this->opted_in_effective  = $opted_in_effective;
    $this->supplier_id         = $supplier_id;
    $this->integration_mode    = $integration_mode;
    $this->id                  = $id;
    $this->obx_id              = $obx_id;
    $this->supplier_obx_id     = $supplier_obx_id;
    $this->supplier_short_name = $supplier_short_name;
  }

  /**
   * Get method for Short_Name
   * @return mixed
   */
  public function getShortName() {
    return $this->short_name;
  }

  /**
   * Get method for Opted_In_Effective
   * @return mixed
   */
  public function getOptedInEffective() {
    return $this->opted_in_effective;
  }



}