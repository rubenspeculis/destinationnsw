// TAB Loader
$('[data-toggle="tabajax"]').click(function (e) {
  var $this = $(this),
    loadurl = $this.attr('href'),
    targ = $this.attr('data-target');

  $.get(loadurl, function (data) {
    $(targ).html(data);
  });

  $this.tab('show');
  return false;
});

// Popup loaded
$('body').on('click', 'a.modalButton', function (e) {
  var src = $(this).attr('data-src');
  var target = $(this).attr('data-target');
  var height = $(this).attr('data-height') || 600;
  var title = $(this).attr('title');

  $(target + " iframe").attr({
                               'src': src,
                               'height': height
                             });
  $(target + " .modal-header h3").html(title);
});

// Set Ajax Indicator
$(document).ajaxStart(function () {
  //show ajax indicator
  ajaxindicatorstart('Loading data... Please wait...');
}).ajaxStop(function () {
//hide ajax indicator
  ajaxindicatorstop();
});