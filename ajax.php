<?php

require_once("inc/autoloader.php");
use DestinationNsw\Process;

// Define variables
$serviceUrl = 'http://www.au.v3travel.com/CABS.WebServices/SearchService.asmx?wsdl';
$channelId  = 'Test_V3_Offload';
$channelKey = '60E0533B-6E38-4779-A7D7-0D82BF824C55';
// Create the process
$process    = new Process($serviceUrl, $channelId, $channelKey);

// Empty providers
$providers = array ();

// Check for the $_GET variable CHOICE. If optedIn then process the filtered list.
if ($_GET[ 'choice' ] === 'optedIn') {
  $providers = $process->optedInEffective();
  $html      = '<ul>';

  foreach ($providers as $provider) {
    $html .= '<li>';
    $html .= '<a class="modalButton" data-toggle="modal" data-src="http://www.au.v3travel.com/CABS2/DiscoveryServices/ProviderAvailability.aspx?exl_dn=';
    $html .= $channelId . '&exl_psn=' . $provider->getShortName() . '"';
    $html .= ' data-target="#modal" title="' . $provider->getShortName() . '" >' . $provider->getShortName();
    $html .= '</a></li>';
  }
  $html .= '</ul>';
}
// If All then return all the results
elseif ($_GET[ 'choice' ] === 'all') {
  $providers = $process->getProviders();
  $html      = '<ul>';
  foreach ($providers as $provider) {
    $html .= '<li>';
    $html .= $provider->getShortName();
    $html .= '</li>';
  }

  $html .= '</ul>';
}


echo $html;