#README

Author: Rubens Peculis 
Email: rubens@rubenspeculis.com
Date: 8th February 2015

This project was written to satisfy the needs of the Destination NSW PHP developer test. The test is defined in the included txa-optin-test.txt file.

##Disclaimer: 
This is to be used as is. No support and no responsibility is taken by the author.
Tested on PHP 5.6.3 using Laravel Homestead development environment.

##Instalation:
The project uses Bower.js to manage front end dependencies. 
To install project you must run: 

>bower install