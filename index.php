<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Destination NSW</title>

  <!-- Bootstrap -->
  <link href="bower_components/bootstrap/dist/css/bootstrap.min.css"
        rel="stylesheet">
  <link href="css/main.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script
    src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
<div class="col-sm-12">
  <h1>Destination NSW</h1>

  <ul class="col-sm-8 col-sm-offset-2 nav nav-tabs tabs-up" id="selector">
    <li><a href="/ajax.php?choice=all" data-target="#all"
           class="media_node active span" id="all_tab" data-toggle="tabajax"
           rel="tooltip"> All </a></li>
    <li><a href="/ajax.php?choice=optedIn" data-target="#opted"
           class="media_node span" id="optedin_tab" data-toggle="tabajax"
           rel="tooltip"> Opted In</a></li>
  </ul>
  <div class="tab-content col-sm-8 col-sm-offset-2">
    <div class="tab-pane active" id="all">
    </div>
    <div class="tab-pane" id="opted">
    </div>
  </div>

</div>

<div class="modal fade" id="modal" tabindex="-1" role="dialog"
     aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">×</button>
      <h3>Title</h3>
    </div>
    <div class="modal-body">
      <iframe src="" style="zoom:0.70" frameborder="0" height="250"
              width="99.6%"></iframe>
    </div>
  </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script
  src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/js/ajaxIndicator.js"></script>
<script src="/js/ajax.js"></script>
</body>
</html>